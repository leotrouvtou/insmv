CREATE TABLE ins_user
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    group_id integer not null,    
    FOREIGN KEY (group_id) REFERENCES ins_group(id),
    PF integer not null,
    bluser integer not null,
    bl integer default 0,
    bg integer default 0,
    bf integer default 0,
    ms integer default 0
)
